#!/bin/bash

DIA=`date +"%d/%m/%Y"`
HORA=`date +"%H:%M"`
FICHERO=`date +%Y%m%d_%H:%M.txt`

mkdir 777 /home/logs -p

echo " "
echo "**********************************************************************"
echo "**********************************************************************"
echo "**                                                                  **"
echo "**   PROGRAMA PARA MONITORIZAR EL HARDWARE Y EL ESTADO DEL EQUIPO   **" 
echo "**    (Muestra la informacion en pantalla y la guarda en un log)    **"
echo "**                                                                  **"
echo "**********************************************************************"
echo "**********************************************************************"
echo " "
echo '#######################' >> /home/logs/$FICHERO
echo '#######################' >> /home/logs/$FICHERO
echo "## " >> /home/logs/$FICHERO
echo "## Fecha: $DIA" >> /home/logs/$FICHERO
echo "## Hora:  $HORA" >> /home/logs/$FICHERO
echo "## " >> /home/logs/$FICHERO
echo "#######################" >> /home/logs/$FICHERO
echo "#######################" >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo "#######################" >> /home/logs/$FICHERO
echo "#                     #" >> /home/logs/$FICHERO
echo "#  NOMBRE DE EQUIPO:  #" >> /home/logs/$FICHERO
echo "#                     #" >> /home/logs/$FICHERO
echo "#######################" >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
hostname >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo "##########" >> /home/logs/$FICHERO
echo "#        #" >> /home/logs/$FICHERO
echo "#  RED:  #" >> /home/logs/$FICHERO
echo "#        #" >> /home/logs/$FICHERO
echo "##########" >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
ifconfig >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo "###########" >> /home/logs/$FICHERO
echo "#         #" >> /home/logs/$FICHERO
echo "#  CPUs:  #" >> /home/logs/$FICHERO
echo "#         #" >> /home/logs/$FICHERO
echo "###########" >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
lscpu >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo "#####################" >> /home/logs/$FICHERO
echo "#                   #" >> /home/logs/$FICHERO
echo "#  ALMACENAMIENTO:  #" >> /home/logs/$FICHERO
echo "#                   #" >> /home/logs/$FICHERO
echo "#####################" >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
fdisk -l >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo "##################" >> /home/logs/$FICHERO
echo "#                #" >> /home/logs/$FICHERO
echo "#  MEMORIA RAM:  #" >> /home/logs/$FICHERO
echo "#                #" >> /home/logs/$FICHERO
echo "##################" >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
free >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo "################" >> /home/logs/$FICHERO
echo "#              #" >> /home/logs/$FICHERO
echo "#  SERVICIOS:  #" >> /home/logs/$FICHERO
echo "#              #" >> /home/logs/$FICHERO
echo "################" >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
service --status-all >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO
echo " " >> /home/logs/$FICHERO

cat /home/logs/$FICHERO