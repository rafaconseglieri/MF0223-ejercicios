# Ejercicios del Módulo Certificado MF0223, Sistemas Operativos y Aplicaciones Informáticas

## Descripción del proyecto:

En el citado módulo se han propuesto 6 ejercicios prácticos. El presente proyecto abarca la solución y documentación de estos ejercicios.

1. Publicar en Internet una aplicación en Bash, para monitorización de host con toda su documentación y API,  
2. Publicar en Internet una aplicación de monitorización de host en ES6, HTML5 y CSS3, para ser usada con Node Webkit; junto con toda la documentación y enlaces necesarios.  
3. Realizar el Documento de Seguridad y Red del Aula y de nuestros recursos online, con toda la documentación y enlaces necesarios.  
4. Publicar en Internet una aplicación en Node.JS automatizada de Copias de Seguridad Basada en el Documento de Seguridad del ejercicio anterior.  
5. Publicar en Internet toda la documentación y y ejercicios del primer módulo formativo usando un sistema de control de versiones.  
6. Publicar en Internet un proyecto web básico de Google Firebase, donde se usen al menos el Hosting y el Storage.

### Formador:
Domingo Muñoz (https://gitlab.com/akira)

### Realizado por:
Rafael Sánchez Conseglieri